# smallbrowser

A small HTTP browser library in Python based on the [requests](https://requests.readthedocs.io/en/master/) library.

## Concept

This library is only composed of five (5) methods.

1. Browser#type(String url)
2. Browser#enter()
3. Browser#fillup(dict form)
4. Browser#submit()
5. Browser#response

Similar to what you do with a browser, you _type_ the URL and press _enter_ to load the URL.
Then, you will get a _response_ back.
When there is a form, you _fill up_ the form and click _submit._

## Usage

The code below will print out the raw HTML of `https://www.google.com` website.

```python
from smallbrowser import Browser

browser = Browser("browser.storage")
response = browser.type("https://www.google.com").enter().response
print(response.text)
```
