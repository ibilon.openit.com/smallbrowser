from setuptools import setup, find_packages

setup(
    name = 'openit-smallbrowser',
    version = '0.3.0',
    packages=find_packages(),
    install_requires=[
        'python-certifi-win32',
        'requests',
        'pyquery',
    ]
)
